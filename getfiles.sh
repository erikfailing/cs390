#!/bin/bash
#
echo $0, $#
if [ $# -ne 2 ]
then
	echo Usage: $0 yyyy mm
	exit 1
fi
yyyy=$1
mm=$2
filelist=$(ls rawdata/*.h5)
output=${yyyy}${mm}.txt
for f in $filelist; do
	filename=${f##*/}
	echo $filename
	year=${filename:7:4}
	month=${filename:11:2}
	if [ $year -eq $yyyy ]; then
		if [ $mm -eq $month ]; then
			echo $f >> $output
		fi
	fi
	#break
done
