ling


Final Exam

CS390 Spring 2020


**************************************************************************************
*
*     All questions are for Linux/Unix systems  
*     This is a open book exam 
*     You are allowed to use your computer and internet
*     You are NOT permitted to discuss with others, including using online chatting!
*     Time: 150 minutes
*
**************************************************************************************


================================================================
Part I (10 pts) True or False Questions (circle True or False)
================================================================

1. (TF) Linux is just another name for UNIX computer system.  They refer to exactly the same operating system

FALSE, Linux is a UNIX-like operating system

2. (TF) Unlike Windows system, File hw6 and Hw6 can coexist in the same directory in LINUX 

TRUE

3. (TF) git is a GNU shell interactive tool developed by gitlab for software version control 

TRUE

4. (TF) UNIX commands must be in lowercase and must not have extension.

TRUE

5. (TF) The results of command echo "$HOME" and echo $HOME are the same.

TRUE

6. (TF) Statement: filelist=$(ls -l) and filelist=`ls -l` are equivalent

TRUE

7. (TF) The outputs of the following commands are exactly the same 
    a. awk '{print NR, NF}' file.cvs 
    b. awk 'END{print NR, NF}' file.cvs
	
FALSE


8. (TF) File log.out will have the exactly same content for command line (a) or (b)
    a. date; pwd; ls > log.out 
    b. (date; pwd; ls) > log.out
	
FALSE


9. (TF) When a file has permission "s", it must have the corresponding "x" permission.

TRUE

10.(TF) In Bash, a defined environmental shell variable is available to every user of the system

TRUE

=====================================================================
 Part II (74 pts) Select only ONE answer for each of the following
=====================================================================

1. What command, followed by the directory name is used to access that specific directory?
Discuss

    A.  cp
    B.  cd
    C.  access
    D.  acs
    E.  cdr

B

2. Which of the following are correct commands for changing the current directory to the user's home?

    A.  cd /home
    B.  cd ..
    C.  cd ~
    D.  cd /
	
C


3. What command do you have to use to go to the parent directory?

    A.  cd -
    B.  cd /up
    C.  cd ~
    D.  cd ..
	
D


4. What command shows the directory you are in?

    A.  pwd
    B.  dir
    C.  directory
    D.  showdir
    E.  dirpwd
	
A


5. Which command shows if /usr/bin is in the current shell search path?

    A.  cat PATH
    B.  echo $PATH
    C.  echo %PATH
    D.  cat $PATH
    E.  echo %PATH%
	
B


6. Which of the following commands moves the directory ~/summer and its content to ~/vacation/2020?

    A.  mv ~/vacation/2020 ~/summer
    B.  move -R ~/summer ~/vacation/2020
    C.  mv /home/summer /home/vacation/2020
    D.  mv ~/summer ~/vacation/2020
    E.  mv -R ~/summer ~/vacation/2020

D

7. Which of the following commands will display all the files in your current directory and its subdirectories including the hidden files?

    A. ls -aR
    B. ls -a
    C. ls -R
    D. ls -l

A

8. Which command lists all files in the current directory that starts with a capital letter?

    A.  ls A-Z
    B.  ls A-Z*
    C.  ls [A-Z]*
    D.  ls --upercasefiles
    E.  ls -uppercase-files
	
C


9. How do you delete a file?

    A.  dl filename
    B.  rm filename
    C.  touch filename
    D.  less filename
	
B


10. Which of the following commands will list all the files ending (the file name) with .out in the current directory and all its subdirectories in bash shell?

    A. find ~ -name "*.out"
    B. ls -R *.out
    C. find . -name "*.out"
    D. grep -r ".out"
	
C


11. Which of the following commands DOES NOT give the context (value) of shell variable A in bash (assume variable A is defined)?

    A. echo "$A"
    B. echo '$A'
    C. echo ${A}
    D. echo $A
	
B


12. Which command is used to make a bash shell variable known to subsequently executed programs (an environmental variable)?

    A.  export
    B.  announce
    C.  env
    D.  transfer
    E.  mv

A

13. Which command would you use to get comprehensive documentation about any command in Linux?

    A.  help command
    B.  echo command
    C.  locate command
    D.  man command
    E.  get command

D

14. What are the three sets of permission for a file?

    A.  user, group, others
    B.  administrator, group, others
    C.  user, standard user, others
    D.  administrator, standard user, others

A

15. Your current unmask is set to 002. If you create a new file, what will the permission of the new file be?

    A. ------w-
    B. rwxrwx-w-
    C. -rw-rw-r-
    D. rwxrwxr-x
	
C
 

16. You need to change the permissions of a file named schedule.txt such that the file owner can edit the file, users who are members of the group that owns the file can edit it, and users who are not owners and don't belong to the owning group can view it but not modify it. Which command will do this?

    A. chmod 664 schedule.txt
    B. chmod 555 schedule.txt
    C. chmod 777 schedule.txt
    D. chmod 644 schedule.txt
	
A


17. Which special permission can't be assigned to a file?

    A. SUID
    B. SGID
    C. sticky bit
    D. None of the above. All special permissions can be assigned to a file
	
D


18. What permissions are set on a file with the command chmod 654 file.txt?

    A.  drw-r-xr--
    B.  d--wxr-x--
    C.  --wxr-x--x
    D.  -rwxrw--x
    E.  -rw-r-xr--
	
E

19. Which command creates an empty file if it does not exist?

    A. cat
    B. touch
    C. ed
    D. read

B

20. What is the command to count the number of characters in a file?

    A. grep
    B. wc
    C. count
    D. cut

B

21. How could one search for the file foo.txt under the directory /home?

    A. search /home -file foo.txt
    B. search /home foo.txt
    C. find /home -file foo.txt
    D. find /home -name foo.txt
    E. find /home foo.txt

D

22. Which of the following commands displays one page of output at a time?

    A. more
    B. sed
    C. pause
    D. grep
	
A


23. Which of the following command displays the current date in the format mm-dd-yyyy?

    A. date +"$m-$d-$Y"
    B. date +%m-%d-%Y
    C. date +/%m-%d-20%y
    D. date +"/%m-%d-20%y"
	
B

24. What does the command "read name" do in bash?

    a. Read only one word from STDIN to variable name.
    b. Send the value of variable name to stdout.
    c. Read a line from STDIN into variable name.
    d. Open file name for read

C

25. What is the output of the following command? tail -n 10 test.txt

    A.  The first 10 lines of test.txt
    B.  The last 10 lines of test.txt omitting the blank lines
    C.  The last 10 lines of test.txt with line numbers
    D.  The last 10 lines of test.txt including the blank lines
	
D


26. Which of the following commands will output all of the lines that contain either the string Fred or fred? 

    A.  frep -v fred data_file
    B.  grep '[f]red' data_file
    C.  egrep fred data_file
    D.  grep '[Ff]red' data_file
    E.  grep -i fred data_file

D

27. You have a program called /user/bin/foo. You wish to create a symbolic link, /home/user/bin/foo, that points to it. Which command will do this task?    
Discuss

    A.  ln -sys /home/user/foo /user/bin/foo
    B.  ln -s /user/bin/foo /home/user/foo
    C.  ln -s /home/user/foo /user/bin/foo
    D.  ln /user/bin/foo /home/user/foo
    E.  ln --symlink /home/user/foo /user/bin/foo
	
B


28. There are four system calls involved during a process creation on a UNIX system, which of the following statement is NOT TRUE?

    a. The new created process (by fork call) will replace the parent process. 
    b. The exec system call will replace the newly created process
    c. The fork system call creates a new process
    d. Normally, the PID of the forked process will be a number larger than the one which makes the fork system call

A

29. In Shell scripting What does does $? Return?

    A.  $? will return exit status of command: Non-0 if command gets successfully executed ,zero if command failed.
    B.  $? will return exit status of command: 0 if command gets successfully executed, non-zero if command failed.
    C.  System error
    D.  Invalid command
	
B

30. Which command will display running process information in realtime?

    A.  top
    B.  show current
    C.  ps current
    D.  process

A

31. What approach does an application use to communicate with the kernel?

    A. C Programs
    B. Shell Script
    C. Shell
    D. System Calls

D

32. In Shell scripting What does $# stand for?

    A.  Will return error number   
    B.  # will return the number of parameters that are passed as the command-line arguments.
    C.  Error
    D.  All the above are incorrect

B

33. What file will be generated after executing command gcc -Wall -O2 qsalary.c on a Linux system?

    A. qsalary.o
    B. qsalary.exe
    C. qsalary.out
    D. a.out

D

34. What file is generated after executing command gcc -c employee.c on a UNIX system?

    A. employee.o
    B. employee.exe
    C. employee.out
    D. a.out

A

35. GNU C compiler has several levels of optimizations such as from Level 0 to level 3. What will happen to the program if optimization level goes up? 

    A. program will run faster (better performance)
    B. a smaller program
    C. reduce compilation time
    D. none of the above

A

36. When using GNU C++ compiler to compile a C++ source code qsalary.cpp, which of the following commands correctly links the external library, libemployee.so which is under /usr/local/lib?

    A. g++ -Wall -O2 -l libemployee.so qsalary.cpp
    B. g++ -Wall -O2 qsalary.cpp -L/usr/local/lib -llibemployee.so
    C. g++ -Wall -O2 -L/usr/local/lib qsalary.cpp -lemployee
    D. g++ -Wall -O2 qsalary.cpp -L/usr/local/lib -llibemployee.so

C

37. On UNIX-like systems, you can compile a program to create the executable against either static libraries or shared libraries. Which of the following statement is correct regarding these two executables?

    A. The one built w/ shared libraries are larger
    B. The one built w/ the static libraries are larger
    C. The one built w/ shared libraries is more portable.
    D. The two executables are exactly the same

B


================================================================
 Part III (16) Short Answers
================================================================

1. (2) What is pipe in UNIX? Explain with an example

Pipe is a unidirectional data channel used for interprocess communication. Data written to the write end of pipe is buffered until it is read from the read end.
$ echo hello | wc -c
give the output 6 because there are 6 characters in hello (including newline)




2. (2) Execute program qsalary in background. Remember to redirect the standard output to file log.out, and the standard error output to file log.err

./qsalary & > log.out & 2> log.err &



3. (2) What is command substitution?  Explain with an example. 

Command substitution is when the output of a command replaces the command.
$(cat file) can be substituted by $(< file)



4. (2) Give a Linux command line to insert a title line *** Spring 2019**** above the first line of file: report.txt

sed -i '1 i\*** Spring 2019****' report.txt




5. (2) A bash script archive.sh is ran as the following: sh archive.sh 2018 10 31. 
   Explain the output of the following commands (in sequence) inside this script: 
   
    a. echo $0 $1 $2 $3 

    b. shift; echo $0 $1 $2 $3 
	
script outputs:
archive.sh 2018 10 31
archive.sh 10 31

the shift command shifts the argument's positions the left, in this case, dropping 2018 because it was in the $1.
Shift doesn't affect $0 (the program name)


6. (2) Explain and define environmental variable LD_LIBRARY_PATH

LD_LIBRARY_PATH is a colon-separated set of directories where libraries are searched for first before the standard set of directories.
It may looks something like this:
/home/user/bin:/home/user/documents/program/bin


7. (2) Explain the following two UNIX commands:

    a) trap "" SIGINT


    b) trap "rm -rf tmp; exit 2" 2 
	
a) does nothing on signal interrupt

b) removes the directory tmp and all of its contents recurvsively, then exits with code 2 if signal interrupt. SIGINT = 2


8. (2)In bash, line="Mike 278 250 100 175 300 20" (Mike and his weekly salary for the pass 6 weeks), give the command line (must be ONE line) to compute and display Mike's weekily average salary in the pass 6 weeks.

awk '{print ($2 + $3 + $4 + $5 + $6 +$7)/6}' <<< "$line"

