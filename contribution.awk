BEGIN{
	FS=":"
}
/[0-9]/{
	count++
	times += NF-2
	for (i=3; i<=NF; i++)
		contributions[$1] += $i

	#print $1, contributions[$1]
}
END{
	total=0;
	for (name in contributions)
	{
		total += contributions[name]
		print name ":", contributions[name]
	}
	print "number of contributors:",count
	print "number of contribuions:",times
	print "total contribution:",total
	print "average contribution pp:", total/count
}
