//By Erik Failing

#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "employee.h"

using namespace std;

int main(int argc, char **argv) 
{ 
	//Ensure only one additional argument was entered
	if (argc != 2)
	{
		printf("ERROR: User did not give ONE command line argument\n");
		return 1;
	}

	//Create vector of employees
	vector<employee> employees;

	//Read database
	ifstream database(argv[1]);
	string fname, lname, sal;
	int count = 0;
	while(database >> fname >> lname >> sal)
	{
		employee e(fname, lname, sal);
		employees.push_back(e);
	}
	database.close();

	/*
	//Print all employee in database
	for (int i = 0; i < employees.size(); i++)
	{
		employees[i].print();
	}
	*/
	
	//Query loop
	string input = "q";
	while (input == "q") 
	{
		//Get user input
		cout << "Enter first name: ";
		cin >> fname;
		cout << "Enter last name: ";
		cin >> lname;
		
		//Search through vector to find user & print out//Print all employee in database
    	for (int i = 0; i < employees.size(); i++)
 		{
 			if(employees[i].fName == fname && employees[i].lName == lname) employees[i].print();
 		}
		
		//Get user input
		cout << "Find another employee's salary (q)? ";
		cin >> input;
	}	
	 
}

