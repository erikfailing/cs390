#Build shared library
#If you get an error finding libemployee.so, try: export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:.

g++ -c -fpic employee.cpp
g++ -shared -o libemployee.so employee.o
g++ -o qsalary_shared qsalary.cpp -L./ -lemployee
