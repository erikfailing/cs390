function numToLetterGrade(num) {
	#Calculate student letter grade
    if (num >= 97) return "A+"
    else if (num >= 94) return "A"
    else if (num >= 90) return "A-"
    else if (num >= 87) return "B+"
    else if (num >= 84) return "B"
    else if (num >= 80) return "B-"
    else if (num >= 77) return "C+"
    else if (num >= 74) return "C"
    else if (num >= 70) return "C-"
    else if (num >= 60) return "D"
    else return "F"
}
BEGIN {
	print "Reading grades from file..."
}

#Skip the first line and only read from non-empty lines
NR>1 && NF>0 {
	#Set current error count so we know if an error occurs when reading the student in
	currentErrorCount = errorCount
	
	#Increase student count by one
	studentCount += 1

	#Read in students and grades
	for (i=2;i<=NF;i+=2) {
		#ERROR: Grade type is not a char/string
		if (!($i~/^[a-zA-Z]+$/)) {
            averages[$1] = "ERROR: Grade type is not a character/string: " $i
            studentErrors[$1] = 1
            errorCount += 1
            break
        }
		
		#ERROR: Grade value is not a number
		if (!($(i+1)~/^[0-9]+$/)) {
			averages[$1] = "ERROR: Grade value is not a number: " $(i+1)
            studentErrors[$1] = 1
            errorCount += 1
            break
		}
		
		#ERROR: Grade value is not with the inclusive range [0-100]
		if ($(i+1) < 0 || $(i+1) > 100) {
			averages[$1] = "ERROR: Grade value not within inclusive range [0-100]: " $(i+1)
			studentErrors[$1] = 1
			errorCount += 1
			break
		}

		#Read grade
		if ($i == "Q" || $i == "q") quizs[$1] = $(i+1)
		else if ($i == "H" || $i == "h") homeworks[$1] = $(i+1)
		else if ($i == "M" || $i == "m") midterms[$1] = $(i+1)
		else if ($i == "F" || $i == "f") finals[$1] = $(i+1)
		else {
			#ERROR: Grade type unknown
			averages[$1] = "ERROR: Unknown grade type found: " $i
			studentErrors[$1] = 1
			errorCount += 1
			break			
		}	
	}
	
	if (currentErrorCount == errorCount) {
		#Calculate student average
		averages[$1] = (quizs[$1] * 0.1) + (homeworks[$1] * 0.4) +(midterms[$1] * 0.2) + (finals[$1] * 0.3)
	
		#Set the error indicator for the student to false
		errors[$1] = 0
	}
}


END {
	#Print the header
	print "---------------------------------------------------------------"
	print "Student Name\t Average\t\tLetter Grade"
	print "---------------------------------------------------------------"

	#Print the students	
	for (student in averages) {
		if (studentErrors[student] > 0) print student, "\t\t", averages[student]
		else print student, "\t\t", averages[student], "\t\t", numToLetterGrade(averages[student])
	}

	#Print the divider
	print "---------------------------------------------------------------"

	#Print Number of Students in the class
	print "The number of students in the class is: ", studentCount

	#Print averages if no errors and error message if there are errors
	if (errorCount > 0) print "CLASS AVERAGES NOT COMPUTED DUE TO ERRORS"
	else {
		#Calculate class averages
		for (student in averages) {
			quizsSum += quizs[student]
			homeworksSum += homeworks[student]
			midtermsSum += midterms[student]
			finalsSum += finals[student]
			averagesSum += averages[student]
		}
		quizsAverage = quizsSum / studentCount
		homeworksAverage = homeworksSum / studentCount
		midtermsAverage = midtermsSum / studentCount
		finalsAverage = finalsSum / studentCount
		averagesAverage = averagesSum / studentCount
		
		#Print class averages
		print "Class average of the quizs: ", "\t\t", quizsAverage, "\t", numToLetterGrade(quizsAverage)
		print "Class average of the homeworks: ", "\t", homeworksAverage, "\t", numToLetterGrade(homeworksAverage)
		print "Class average of the midterm: ", "\t\t", midtermsAverage, "\t", numToLetterGrade(midtermsAverage)
		print "Class average of the final: ", "\t\t", finalsAverage, "\t", numToLetterGrade(finalsAverage)
		print "Class average grade: ", "\t\t\t", averagesAverage, "\t", numToLetterGrade(averagesAverage)
	}
}

