#!/bin/bash

#Make sure the user entered two arguments, if not error and print Usage
if [ $# -ne 2 ]; then
        echo -e "ERROR - Usage: bash $0 awkscript report.txt\n"
        exit 2
fi

#Get variables from user input
awkscript=$1
outfile=$2
printoutfile=`cat $outfile`

#Make sure awkscript exists, if not error and print message
if [ ! -f $awkscript ]; then
        echo "ERROR: $awkscript does not exist\n"
        exit 3
fi

#If the outfile already exists
if [ ! -f $outfile ]; then
        echo -e "No $outfile found, generating new report file."
        echo "                *****************************" >> $outfile
        echo "                     Class Grade Report      " >> $outfile
        echo "                         Spring 2020         " >> $outfile
        echo "                *****************************" >> $outfile
        echo -e "Name\tQuiz\tHomework\tMidterm\tFinal\tScore\tGrade" >> $outfile
else
        echo -e "Report:"
        echo $printoutfile
fi

echo -e  "Enter a student record (y/n)?"
read answer
if [ "$answer" != "y" ]; then
        echo -e "Print contents of current report..."
        echo $printoutfile
        exit 4
fi

types=( Q H M F )
echo -n "Student Name: "
read name
line=$name

#See if student already exists in file
if [ -f $outfile ]; then
		if grep -q $name "$outfile"; then
			echo -e "Student already exists in report"
			grep $name "$outfile"
		fi

		#Propmt user if they would like to update student
		echo -e "Update student record (y/n)?"
		read update
		if [ "$update" == "y" ]; then
			sed -i "/$name/d" $outfile
		else
			exit 7
		fi
fi
# Initialize number checking variable
numcheck='^[0-9]+$'

# Initialize scores loop
for type in ${types[@]}; do #use @ to cycle through array
		echo -n "Enter grade of type $type:"
		read score

		# Validate entered score for numerical input
		if ! [[ $score =~ $numcheck ]]; then
				echo "ERROR: Input contains non-numerical characters"
				exit 5
		fi

		if [[ "$score" -gt 100 || "$score" -lt 0 ]]; then
				echo "ERROR: Input not within [0-100]"
				exit 6
		fi
		line="$line $score"
done

#Compute student's grade & append the student to the outfile
grade=$(echo $line |awk -f $awkscript)
echo -e "Student Data: $grade"
report=($grade)
echo -e "${report[0]}\t${report[1]}\t${report[2]}\t\t${report[3]}\t${report[4]}\t${report[5]}\t${report[6]}" >> "$outfile"



