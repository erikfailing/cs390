#/bin/bash
# This script tries to check the existence of files listed
# in list.txt.  If it exists and it's a directory, the contents
# of the directory will be listed.
#
#  

for foo in $(cat filelist.txt); do
	if [[ -f $foo ]] || [[ -d $foo ]]; then
		echo Jackpot! $foo exists
        #check if this existing file is a directory
    	if [[ -d $foo ]]; then 
        	echo "$foo is a directory, here is its content"
        	echo "***************************"
        	ls -1 $foo
        	echo "***************************"
		fi
	else
		echo File $foo doesn’t exist
	fi
done

echo DONE

